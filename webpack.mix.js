const { mix } = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // .webpackConfig({
    //     module: {
    //         rules: [
    //             {
    //                 enforce: 'pre',
    //                 exclude: /node_modules/,
    //                 loader: 'eslint-loader',
    //                 test: /\.(js|vue)?$/,
    //                 options: {
    //                     fix: true,
    //                 },
    //             },
    //         ],
    //     },
    // })
    .js('src/assets/js/app.js', 'public/js')
    .less('src/assets/less/app.less', 'public/css')
    .copyDirectory('src/assets/fonts/icomoon/fonts', 'public/fonts')

if (mix.config.inProduction) {
    mix.version()
}
