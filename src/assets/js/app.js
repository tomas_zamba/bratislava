// eslint-disable-next-line
import WOW from 'wow.js'

import '../vendor/bootstrap/js/bootstrap'
import './components/rypp'

import CookiesAgreement from './components/cookiesAgreement'
import Hello from './components/hello'
import Menu from './components/menu'
import Search from './components/search'
import SlideMenu from './components/SlideMenu'
import Socials from './components/socials'
import Swiper from './components/swiper'

const apiKey = 'AIzaSyA0lPWSVLfZG2mjLmqaYMBL-6RaFu1lWCY'

CookiesAgreement.init()
Menu.init()
Search.init()
SlideMenu.init()
Swiper.init()
Socials.init()

$(document).ready(() => {
    $('.RYPP').rypp(apiKey, {
        autoplay: false,
    })

    if ($.fn.datepicker) {
        $('.datetimepicker').datepicker({
            format: 'dd.mm.yyyy',
            language: 'sk',
            autoclose: true,
        })
    }

    $('[data-city]').hover(function () {
        const $el = $(this)
        const cityPart = $el.data('city')
        $(`[data-city=${cityPart}`).toggleClass('active')
    })

    $('[data-href]').click(function () {
        const $el = $(this)
        window.location.href = $el.data('href')
    })

    new WOW({
        offset: 50,
        live: true,
    }).init()

    Hello.world()

    $('.form-control').on('input change', function () {
        const $el = $(this)
        $el.parents('.input-group').first().toggleClass('filled', $el.val().trim().length > 0)
    })

    $('[data-toggle="tooltip"]').tooltip()
})
