export default {
    menu: null,
    menuGenerated: null,
    back: null,

    init() {
        const self = this
        this.menu = $('.js-menu-mobile')
        this.menuGenerated = $('.js-menu-mobile-generated')
        this.back = $('.js-menu-mobile-back')

        this.menu.find('li').each(function () {
            const $el = $(this)
            $el.toggleClass('has-submenu', $el.find('ul').length > 0)
        })

        this.removeClasses()
        this.menu.addClass('active-submenu')
        this.checkBackButton()

        this.menu.find('a').on('click', function (e) {
            const $link = $(this)
            const $menu = $link.parent('li')
            const $submenu = $link.siblings('ul')
            const $parents = $link.parents('li')

            if ($menu.hasClass('active-menu') || $submenu.length === 0) {
                return true
            }

            e.preventDefault()

            self.removeClasses()

            $parents.addClass('active-parent')

            if ($menu.length > 0) {
                $menu.addClass('active-menu')
            }
            if ($submenu.length > 0) {
                $submenu.addClass('active-submenu')
            }

            self.checkBackButton()
        })

        this.back.on('click', e => {
            e.preventDefault()
            const $menu = $('.active-menu')
            const $submenu = $('.active-submenu')

            const $newMenu = $menu.parents('li').first()
            const $newSubmenu = $submenu.parents('ul').first()
            const $parents = $menu.parents('li')

            self.removeClasses()

            $parents.addClass('active-parent')

            if ($newMenu.length > 0) {
                $newMenu.addClass('active-menu')
            }
            if ($newSubmenu.length > 0) {
                $newSubmenu.addClass('active-submenu')
            } else {
                this.menu.addClass('active-submenu')
            }

            self.checkBackButton()
        })
    },

    removeClasses() {
        const $menus = this.menu.find('li')
        const $submenus = this.menu.find('ul')

        $menus.removeClass('active-menu')
        $menus.removeClass('active-parent')
        $submenus.removeClass('active-submenu')
        this.menu.removeClass('active-submenu')
    },

    checkBackButton() {
        this.back.toggleClass('active-submenu', !this.menu.hasClass('active-submenu'))
    },
}
