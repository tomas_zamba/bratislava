const Menu = {
    $isMenuOpen: false,
    $menu: null,
    $menuItems: null,

    init() {
        const self = this
        this.$menu = $('.js-menu')
        this.$menuItems = $('.js-menu__item')

        /**
         * Hover intent
         */
        var cX,
            cY,
            _cfg = { interval: 100, sensitivity: 6, timeout: 0 },
            INSTANCE_COUNT = 0,
            track = function (ev) {
                cX = ev.pageX, cY = ev.pageY
            },
            compare = function (ev, $el, s, cfg) {
                if (Math.sqrt((s.pX - cX) * (s.pX - cX) + (s.pY - cY) * (s.pY - cY)) < cfg.sensitivity) return $el.off(s.event, track), delete s.timeoutId, s.isActive = !0, ev.pageX = cX, ev.pageY = cY, delete s.pX, delete s.pY, cfg.over.apply($el[0], [ev])
                s.pX = cX, s.pY = cY, s.timeoutId = setTimeout(() => {
                    compare(ev, $el, s, cfg)
                }, cfg.interval)
            },
            delay = function (ev, $el, s, out) {
                return delete $el.data('hoverIntent')[s.id], out.apply($el[0], [ev])
            }
        $.fn.hoverIntent = function (handlerIn, handlerOut, selector) {
            let instanceId = INSTANCE_COUNT++,
                cfg = $.extend({}, _cfg)
            $.isPlainObject(handlerIn) ? (cfg = $.extend(cfg, handlerIn), $.isFunction(cfg.out) || (cfg.out = cfg.over)) : cfg = $.isFunction(handlerOut) ? $.extend(cfg, {
                over: handlerIn,
                out: handlerOut,
                selector,
            }) : $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut })
            const handleHover = function (e) {
                let ev = $.extend({}, e),
                    $el = $(this),
                    hoverIntentData = $el.data('hoverIntent')
                hoverIntentData || $el.data('hoverIntent', hoverIntentData = {})
                let state = hoverIntentData[instanceId]
                state || (hoverIntentData[instanceId] = state = { id: instanceId }), state.timeoutId && (state.timeoutId = clearTimeout(state.timeoutId))
                const mousemove = state.event = `mousemove.hoverIntent.hoverIntent${instanceId}`
                if (e.type === 'mouseenter') {
                    if (state.isActive) return
                    state.pX = ev.pageX, state.pY = ev.pageY, $el.off(mousemove, track).on(mousemove, track), state.timeoutId = setTimeout(() => {
                        compare(ev, $el, state, cfg)
                    }, cfg.interval)
                } else {
                    if (!state.isActive) return
                    $el.off(mousemove, track), state.timeoutId = setTimeout(() => {
                        delay(ev, $el, state, cfg.out)
                    }, cfg.timeout)
                }
            }
            return this.on({
                'mouseenter.hoverIntent': handleHover,
                'mouseleave.hoverIntent': handleHover,
            }, cfg.selector)
        }

        /**
         * Toggle dropdown menu
         */
        this.$menuItems.hoverIntent({
            over: function mouseOver() {
                const $item = $(this)
                const $dropDown = $item.find('.mega-dropdown-menu')

                self.$isMenuOpen = true

                setTimeout(checkMenuHeight)

                $item.addClass('active')

                $dropDown.finish()
                    .css('display', 'flex')
                    .hide()
                    .fadeIn()
            },
            out: function mouseLeave() {
                const $item = $(this)
                const $dropDown = $item.find('.mega-dropdown-menu')

                self.$isMenuOpen = false

                checkMenuHeight()

                $item.removeClass('active')

                $dropDown.finish()
                    .fadeOut(0)
            },
            timeout: 100,
        })

        const checkMenuHeight = () => {
            $('li.active .mega-dropdown-menu ul').css('height', '')
            $('.js-menu-bgr').css('height', '')

            const $submenus = self.$menu.find('li.active > ul, li.active .mega-dropdown-menu__content')
            let height = 0
            $submenus.each(function () {
                const $elem = $(this)
                const h = $elem.height()
                if (h > height) {
                    height = h
                }
            })
            // setTimeout(() => {
            $('li.active .mega-dropdown-menu ul').height(height)
            $('.js-menu-bgr').height(self.$isMenuOpen ? height : 0)
            // }, 300)
        }

        /**
         * Toggle dropdown mega CONTENT
         */
        const $secondLevel = this.$menuItems.find('.mega-dropdown-menu > ul > li')
        $secondLevel.hoverIntent(function mouseOver() {
            const $item = $(this)
            const $content = self.$menu.find('.mega-dropdown-menu__content')
            const $dropDown = $item.find('ul')

            if ($dropDown.length > 0) {
                $content.finish().fadeOut(0)
            }
        }, function mouseLeave() {
            const $item = $(this)
            const $content = self.$menu.find('.mega-dropdown-menu__content')
            const $dropDown = $item.find('ul')

            if ($dropDown.length > 0) {
                $content.finish().fadeIn()
            }
        })

        /**
         * Toggle submenus
         */
        const $submenu = this.$menuItems.find('.mega-dropdown-menu li')
        $submenu.hoverIntent({
            over: function mouseOver() {
                const $item = $(this)
                const $dropDown = $item.find('ul').first()

                $item.addClass('active')

                checkMenuHeight()

                $dropDown.finish().fadeIn()
            },
            out: function mouseLeave() {
                const $item = $(this)
                const $dropDown = $item.find('ul').first()

                $item.removeClass('active')

                checkMenuHeight()

                $dropDown.finish().fadeOut(0)
            },
            timeout: 100,
        })
    },
}

export default Menu
