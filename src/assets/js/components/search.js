const Search = {
    $search: null,
    $searchForm: null,
    $searchClose: null,

    init() {
        const self = this

        this.$search = $('.menu-primary > li:first-child a, .menu-primary > li:first-child span, .js-mobile-search')
        this.$searchForm = $('#layout-header .widget-search-form')
        this.$searchClose = $('.js-search-form-close')

        /**
         * Toggle search
         */
        this.$search.on('click', e => {
            e.preventDefault()
            self.$searchForm.fadeIn()
            self.$searchForm.find('input').focus()
            this.$search.parents('.header__login').addClass('search--active')

            document.addEventListener('keyup', searchKeyUp)
        })

        this.$searchClose.on('click', () => {
            self.$searchForm.fadeOut()
            this.$search.parents('.header__login').removeClass('search--active')

            document.removeEventListener('keyup', searchKeyUp)
        })

        function searchKeyUp(e) {
            if (e.keyCode === 27) {
                self.$searchClose.click()
            }
        }
    },
}

export default Search
