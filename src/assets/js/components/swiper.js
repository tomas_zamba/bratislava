import Swiper from '../../vendor/swiper/js/swiper'

const isIE = () => {
    const ua = window.navigator.userAgent
    const msie = ua.indexOf('MSIE ')

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        // If Internet Explorer, return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)))
    }
    // If another browser, return 0
    return false
}

export default {
    prevOffset: 150,
    nextOffset: 150,

    initGallery() {
        const $galleries = $('.js-swiper-gallery')

        $galleries.each(function () {
            const $gallery = $(this)
            const $top = $gallery.find('.js-swiper-gallery-top')
            const $thumbs = $gallery.find('.js-swiper-gallery-thumbs')

            const galleryTop = new Swiper($top, {
                nextButton: this.querySelector('.js-swiper-gallery-next'),
                prevButton: this.querySelector('.js-swiper-gallery-prev'),
                spaceBetween: 2,
                initialSlide: 2,
            })

            const galleryThumbs = new Swiper($thumbs, {
                spaceBetween: 2,
                centeredSlides: true,
                slidesPerView: 'auto',
                touchRatio: 0.2,
                slideToClickedSlide: true,
                initialSlide: 2,
            })

            galleryTop.params.control = galleryThumbs
            galleryThumbs.params.control = galleryTop
        })
    },

    initFree() {
        const $el = $('.js-swiper-free')
        const $swiperElement = $el.find('.js-swiper-free-container')

        const defaultOptions = {
            slidesPerView: 'auto',
            nextButton: '.js-swiper-info-next',
            prevButton: '.js-swiper-info-prev',
            freeMode: true,
        }

        const $swiper = new Swiper($swiperElement, defaultOptions)
    },

    initInfo() {
        const $el = $('.js-swiper-info')
        const $swiperElement = $el.find('.js-swiper-info-container')

        const defaultOptions = {
            loop: true,
            slidesPerView: 5,
            nextButton: '.js-swiper-info-next',
            prevButton: '.js-swiper-info-prev',
            onlyExternal: true,
            breakpoints: {
                // when window width is <= 480px
                480: {
                    slidesPerView: 1,
                    onlyExternal: false,
                },
                // when window width is <= 640px
                640: {
                    slidesPerView: 2,
                    onlyExternal: false,
                },
                // when window width is <= 768px
                991: {
                    slidesPerView: 3,
                    onlyExternal: false,
                },
            },
        }

        const $swiper = new Swiper($swiperElement, defaultOptions)
    },

    init() {
        this.initInfo()
        setTimeout(() => {
            this.initFree()
        }, 1000)
        this.initGallery()

        const self = this
        const $el = $('.js-swiper')
        const $swiperElement = $el.find('.js-swiper-container')
        const $wrapper = $swiperElement.find('.swiper-wrapper')
        const autoplay = parseInt($el.data('autoplay'), 10)
        console.log('autoplay', autoplay)

        this.$next = $('.js-swiper-next')
        this.$prev = $('.js-swiper-prev')

        let side
        let isOffset = false
        let originalTransform

        const removeOffset = () => {
            if (isOffset === false) return

            isOffset = false
            $('.js-swiper-pagination').finish().fadeIn(0)
            $wrapper.css('transform', `translate3d(${originalTransform}px,0,0)`)
        }
        let transitioning = false
        const defaultOptions = {
            autoplay,
            loop: false, // Problem with loop is that it jumps on transition when last slide offset
            centeredSlides: true,
            slidesPerView: 'auto',
            paginationClickable: true,
            pagination: '.js-swiper-pagination',
            nextButton: '.js-swiper-next',
            prevButton: '.js-swiper-prev',
            speed: 700,
            onTouchStart() {
                transitioning = true
                $('.js-swiper-pagination').finish().fadeOut(0)
            },
            onTouchEnd() {
                transitioning = false
                $('.js-swiper-pagination').finish().fadeIn(0)
            },
            onSlideChangeStart() {
                removeOffset()
            },
            onTransitionStart() {
                transitioning = true
            },
            onTransitionEnd() {
                transitioning = false
                if (side === 'prev') {
                    self.$prev.trigger('mouseenter')
                } else if (side === 'next') {
                    self.$next.trigger('mouseenter')
                }
            },
        }

        const $swiper = new Swiper($swiperElement, defaultOptions)

        if (window.innerWidth < 768) return
        /**
         * Swiper hover effect
         */
        const doOffset = prev => {
            if (transitioning) {
                return
            }

            originalTransform = parseInt($wrapper.css('transform').split(',')[4])
            const offset = (prev === true) ? originalTransform + this.prevOffset : originalTransform - this.nextOffset

            isOffset = true
            side = (prev === true) ? 'prev' : 'next'
            $wrapper.css('transform', `translate3d(${offset}px,0,0)`)
            $wrapper.css('transition-duration', '.3s')
            $('.js-swiper-pagination').finish().fadeOut(0)
        }

        this.$prev.on('mouseenter', () => {
            if ($('.js-swiper-prev').hasClass('swiper-button-disabled')) return
            doOffset(true)
        })

        this.$next.on('mouseenter', () => {
            if ($('.js-swiper-next').hasClass('swiper-button-disabled')) return
            doOffset(false)
        })

        this.$next.on('mouseleave', () => {
            side = null
            removeOffset()
        })
        this.$prev.on('mouseleave', () => {
            side = null
            removeOffset()
        })
    },
}
