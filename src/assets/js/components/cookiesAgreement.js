const Cookies = {
    set(key, value) {
        window.localStorage.setItem(key, value)
    },
    get(key) {
        return window.localStorage.getItem(key)
    },
}

const CookiesAgreement = {
    lang: '',
    cookieName: '',
    element: document.getElementById('cookie-agreement'),
    elementBtn: document.getElementById('cookie-agreement-btn'),
    init() {
        if (!this.element || !this.elementBtn) {
            console.error('Missing #cookie-agreement or #cookie-agreement-btn element.')
            return
        }

        this.lang = document.documentElement.lang || 'sk'
        this.cookieName = `cookie-agreement-${this.lang}`

        const cookieAgreement = Cookies.get(this.cookieName)
        if (cookieAgreement === null) {
            this.showMessage()
        } else {

        }
    },
    showMessage() {
        const self = this

        this.element.classList.add('cookie-agreement--show')
        this.elementBtn.addEventListener('click', e => {
            e.preventDefault()
            self.agree()
        })
    },
    agree() {
        Cookies.set(this.cookieName, 'check')
        this.element.classList.remove('cookie-agreement--show')
    },
}

export default CookiesAgreement
