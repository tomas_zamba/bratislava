export default {

    init() {
        // this.initFacebook()
    },

    initFacebook() {
        const share = $('.js-share-facebook')
        share.on('click', function () {
            const $el = $(this)

            FB.ui({
                method: 'share',
                href: 'https://developers.facebook.com/docs/',
            }, response => {
                console.log('res', response)
            })
        })
    },
}
