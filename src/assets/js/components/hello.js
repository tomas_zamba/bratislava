const Hello = {

    world() {
        $('.page-preload').find('#layout-header, #layout-before-main').fadeTo(500, 1)
        setTimeout(() => {
            $('.page-preload').find('#layout-main-container').fadeTo(400, 1)
        }, 300)
    },

}

export default Hello
